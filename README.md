# Integration Document 
Identomat library provides a user identification flow that consists of different steps and involves 
the combination of document type and user image/video.



### Following steps are necessary to start a session with identomat API

1. Get a session token

2. Configure Flags

3. Get the instance of identomat SDK

4. Pass Session Token and Handle callback

5. Start identomat SDK

6. Pass additional variables
 
 


### 1. Get a session token
Every identification process has it's session token, to start sdk we first need to generate a token.
To generate session we need Company key. Company key is a String key acquired from Identomat.
You have to generate session token on server side and send it to client. Do not put company key in the app.
For only demonstration we're generating session token in the example app.

Session token request is following:
url: `<domain>/external-api/begin`
url parameters:
    company_key
    flags

flags is json object which need to be converted to String and Encoded as url safe String.
example:
```
String url = BASE_URL + SESSION_URL +"?"+ COMPANY_KEY_URL + COMPANY_KEY;

static HashMap flags = new HashMap() {{
    put("language", "en");
    put("liveness", true);
    put("allow_document_upload", true);
    put("skip_document", false);
    put("skip_face", true);
    put("require_face_document", false);
    put("skip_agreement", false);
    put("server", "computer");

    put("document_type_id", true);
    put("document_type_passport", true);
    put("document_type_driver_license", true);
    put("document_type_residence_license", true);
    put("document_type_non_bio_passport", true);

    put("allow_general_document_capture", true);
    put("allow_general_document_upload", true);

}};
url += "&flags=" + new JSONObject(flags).toString();
``` 
The response from the API call above is a session token that needs to be passed to the instance of Identomat object.

### 2. Configure Flags

Passed Flags define SDK's functionality, here is what every flag do:

    "language" : string - defines the language of sdk - allowed : "en", "ka", "ru", "es", "uk", "uz"
    "liveness" : boolean - defines the method of liveness, (true - liveness check, false - match to photo method)
    "allow_document_upload" : boolean - allows user to upload document instead of scanning.
    "skip_document" : boolean - skips document capture step and goes to face capture
    "skip_face" : boolean - skips face capture step
    "require_face_document" : boolean - additional step where user is required to capture himself holding document
    "skip_agreement" : boolean - skips agreement page
    "server" : String : "human" - makes call to operator, "computer" uses default sdk way.
    
    "document_type_id" : boolean,
    "document_type_passport" : boolean,
    "document_type_driver_license" : boolean,
    "document_type_residence_license" : boolean,
    
     "allow_general_document_capture" : boolean,
     "allow_general_document_upload" : boolean,


### 3. Get the instance of identomat SDK and pass Data

Main class of identomat SDK is `IdentomatManager` class.
`IdentomatManager` is singleton class of identomat SDK (Kotlin's Object class).
To get the SDK variable you can do following: 
For java you can get the instance by doing following:
`IdentomatManager identomatSdk = IdentomatManager.INSTANCE`
If you are using kotlin you can use it as `IdentomatManager`, it's Kotlin's Object class. 

### 4. Pass Session Token and Handle callback

`sessionToken`  -- response from API : `String` type
To pass session token we have `setUp` function which can be used like this:
`IdentomatManager.INSTANCE.setUp(token: sessionToken)`
Last thing we need to do is to pass callback function which will be called after user finishes interacting with library. for that we have
`.setCallback` function which takes function and argument
``` 
IdentomatManager.INSTANCE.setCallback(() -> {
    //go to result page;
});

```

### 5. Start identomat SDK
Library will give us Intent of the SDK's main Activity, which we can start like this:
```
startActivity(IdentomatManager.INSTANCE.getIdentomatActivity(MainActivity.this));
```

### 6. Pass additional variables
To customize identomat library for the app we have some additional functions: 
1. `setColors(colors: Map)` - sets specific colors 
2. `setStrings(dict : Map)` - sets specific text strings
3. `setVariables(variables : Map<String, Any?>)` - sets many customizable variable
4. `setLogo(() -> View?)` - sets loading animation, takes function type, which returns view, example usage:



Following functions are DEPRECATED after 1.0.21 version:

5.0  `setTitleFont(font : Typeface?, size : Int)` - sets title font in library

5.1  `setHead1Font(font: Typeface, fontsize: Int)` - sets header font in library

5.2  `setHead2Font(font: Typeface, fontsize: Int)` - sets sub header font in library

5.3  `setBodyFont(font: Typeface, fontsize: Int)` - sets body text font in library

5.4  `skipLivenessInstructions()` - skips liveness instructions

5.5  `setLivenessIcons(neutralFace: Int, smileFace: Int)` - sets liveness icons, send resource int like this: `R.drawable.ic_liveness_neutral_icon`

5.6  `setLivenessRetryIcon(retryIcon: Int?, size : Int)` - sets liveness retry panel's main icon, size sets size of icon

5.7  `setRetryIcon(retryIcon: Int?, size : Int)` - sets document scan view's retry panel's main icon, size sets size of icon

5.8  `setCameraDenyIcon(cameraDenyIcon: Int?, size : Int)` - sets document scan view's retry panel's main icon

5.9  `setButtonCornerRadius(radius : Int)` - sets every button corner radius

5.10 `setPanelElevation(elevation : Int)` - sets panels elevations



#1.
to fit library's color with the application colors we have `setColors` function,
we need to pass dictionary to this function, with specific keys and hex colors,
```
"background_low",
"background_high",
"text_color_title"
"text_color_header",
"text_color",
"primary_button",
"primary_button_text",
"secondary_button",
"secondary_button_text",
"document_outer",
"loading_indicator_background",
"iteration_text",
```

Example of dictionary:
```
//java
static HashMap colors = new HashMap(){{
    put("background_low", "#222222");
    put("background_high", "#2D2D2D");
    put("text_color_header", "#A4A4A4");
    put("text_color", "#676767");
    put("primary_button", "#7848FF");
    put("primary_button_text", "#FFFFFF");
    put("secondary_button", "#222222");
    put("secondary_button_text", "#FFFFFF");
    put("document_outer", "#2D2D2D");
    put("iteration_text", "#FFFFFF");
}};
//kotlin
 var colors : Map<String, String> = mapOf(
    "background_low" to "#222222",
    "background_high" to "#FFFFFF",
    ...
    )
```
Way to pass colors to library:
`IdentomatManager.setColors(colors: colors)`


#2. 
to customize the strings that library displays at specific places we have function `setStrings(string : Map)`
 `setStrings` function passes Map with specific keys for different languages, structure looks like this:
```
//java
static HashMap strings = new HashMap(){{
    put("en",  new HashMap<String, String>() {{
        put("identomat_agree", "Yes I agree");
        put("identomat_disagree", "Disagree");
    }});
    put("ru",  new HashMap<String, String>() {{
        put("identomat_agree", "Согласен");
        put("identomat_disagree", "Не согласен");
    }});
    put("es",  new HashMap<String, String>() {{
        put("identomat_agree", "Acepto");
        put("identomat_disagree", "No acepto");
    }});
    put("ka",  new HashMap<String, String>() {{
        put("identomat_agree", "ვეთანხმები");
        put("identomat_disagree", "არ ვეთანხმები");
    }});
}};

//kotlin
 var strings : Map<String, Map<String,String>> = mapOf(
            "en" to mapOf(
                    "identomat_agree" to "Yes I agree",
                    "identomat_disagree" to "Disagree",
            ),
            "ru" to mapOf(
                    "identomat_agree" to "Согласен",
                    "identomat_disagree" to "Не согласен",
            ),
            "es" to mapOf(
                    "identomat_agree" to "Acepto",
                    "identomat_disagree" to "No acepto",
            ),
            "ka" to mapOf(
                "identomat_agree" to "ვეთანხმები",
                "identomat_disagree" to "არ ვეთანხმები",
            )
    )
```
Every string has it's key in library and it can be changed. String keys list is at the end. 

#3. 
For every other customization we have `setVariables(variables : Map<String, Any?>)` function, 
where we pass key value type variable. map looks like this:

```
var variables : MutableMap<String, Any?> = mutableMapOf(
   
    "liveness_neutral_icon" to R.drawable.ic_liveness_neutral_icon,     -- sets liveness neutral face icon.     type -> int
    "liveness_smile_icon" to R.drawable.ic_liveness_smile_icon,         -- sets liveness smile face icon.       type -> int

    "liveness_retry_icon" to R.drawable.image,                          -- sets liveness retry page icon.       type -> int
    "scan_retry_icon" to R.drawable.image,                              -- sets scan document retry page icon.  type -> int
    "camera_deny_icon" to R.drawable.image,                             -- sets camera deny page icon.          type -> int
    "upload_retry_icon" to R.drawable.image,                            -- sets upload retry page icon.         type -> int

    "liveness_retry_icon_size" to 200,                                  -- sets save icon sizes.                type -> int
    "scan_retry_icon_size" to 200,
    "camera_deny_icon_size" to 200,
    "upload_retry_icon_size" to 200,

    "skip_liveness_instructions" to false,              -- skips liveness instructions                          type -> boolean
    "liveness_type" to 1,                               -- chooses liveness icons display type values 1 or 2    type -> int
    "button_corner_radius" to null,                     -- sets button corner radious, if it's null button will be round cornered              
    "panel_elevation" to 1,                             -- sets panels elevation,                               type -> int

    "title_font" to null,                               --sets title font                           type -> Typeface?
    "head1_font" to null,                               --sets head1 font                           type -> Typeface?
    "head2_font" to null,                               --sets head2 font                           type -> Typeface?
    "body_font" to null,                                --sets body font                            type -> Typeface?

    "title_font_size" to 20,                            --sets same font sizes                      type -> Int
    "head1_font_size" to 20,
    "head2_font_size" to 16,
    "body_font_size" to 11,
)
```


String keys:
```
<resources>

    <string name="identomat_agree">Agree</string>
    <string name="identomat_title"> </string>
    <string name="identomat_agree_page_title">Consent for personal data processing</string>

    <string name="identomat_capture_method_title">Choose a method</string>

    <string name="identomat_card_front_instructions">Scan FRONT SIDE of ID CARD</string>
    <string name="identomat_card_front_upload">Upload FRONT SIDE of ID CARD</string>
    <string name="identomat_card_looks_fine">Card looks fine</string>
    <string name="identomat_card_rear_instructions">Scan BACK SIDE of ID CARD</string>
    <string name="identomat_card_rear_upload">Upload BACK SIDE of ID CARD</string>

    <string name="identomat_choose_file">Choose a file</string>

    <string name="identomat_continue">Continue</string>
    <string name="identomat_disagree">Disagree</string>

    <string name="identomat_driver_license">Driver license</string>
    <string name="identomat_driver_license_front_instructions">Scan FRONT SIDE of DRIVER LICENSE</string>
    <string name="identomat_driver_license_front_upload">Upload FRONT SIDE of DRIVER LICENSE</string>
    <string name="identomat_driver_license_rear_instructions">Scan BACK SIDE of DRIVER LICENSE</string>
    <string name="identomat_driver_license_rear_upload">Upload BACK SIDE of DRIVER LICENSE</string>

    <string name="identomat_ukr_driver_license">Ukrainian Driver license</string>
    <string name="identomat_ukr_driver_license_front_instructions">Scan FRONT SIDE of DRIVER LICENSE</string>
    <string name="identomat_ukr_driver_license_front_upload">Upload FRONT SIDE of DRIVER LICENSE</string>
    <string name="identomat_ukr_driver_license_rear_instructions">Scan BACK SIDE of DRIVER LICENSE</string>
    <string name="identomat_ukr_driver_license_rear_upload">Upload BACK SIDE of DRIVER LICENSE</string>

    <string name="identomat_face_instructions">Place your FACE within OVAL</string>
    <string name="identomat_face_looks_fine">Face looks fine</string>

    <string name="identomat_card">ID card</string>
    <string name="identomat_im_ready">I\'m ready</string>
    <string name="identomat_initializing">Initializing...</string>
    <string name="identomat_lets_try">Let\'s try</string>

    <string name="identomat_passport">Passport</string>
    <string name="identomat_passport_instructions">Passport photo page</string>
    <string name="identomat_passport_upload">Upload passport photo page</string>
    <string name="identomat_ukr_passport">Ukrainian passport</string>

    <string name="identomat_record_begin_section_1">Take a neutral expression</string>
    <string name="identomat_record_begin_section_2">Smile on this sign</string>
    <string name="identomat_record_begin_section_3">Take a neutral expression again</string>
    <string name="identomat_record_begin_title">Get ready for your video selfie</string>

    <string name="identomat_record_fail_description">But first, please take a look at the instructions</string>
    <string name="identomat_record_fail_title">Let\'s try again</string>
    <string name="identomat_record_instructions">Place your FACE within OVAL and follow the on-screen instructions</string>

    <string name="identomat_residence_permit">Residence permit</string>
    <string name="identomat_residence_permit_front_instructions">Scan FRONT SIDE of RESIDENCE PERMIT</string>
    <string name="identomat_residence_permit_front_upload">Upload FRONT SIDE of RESIDENCE PERMIT</string>
    <string name="identomat_residence_permit_rear_instructions">Scan BACK SIDE of RESIDENCE PERMIT</string>
    <string name="identomat_residence_permit_rear_upload">Upload BACK SIDE of RESIDENCE PERMIT</string>


    <string name="identomat_inn">Inn photo page</string>
    <string name="identomat_scan_inn">Scan INN Document</string>

    <string name="identomat_retake_photo">Retake photo</string>
    <string name="identomat_retry">Retry</string>
    <string name="identomat_scan_code">Scan the code</string>
    <string name="identomat_scan_me">Scan me</string>
    <string name="identomat_select_document">Select document</string>
    <string name="identomat_neutral_expression">Neutral face</string>
    <string name="identomat_smile">Smile</string>
    <string name="identomat_take_photo">Take a photo</string>
    <string name="identomat_upload_another_file">Upload another file</string>

    <string name="identomat_upload_file">Upload a file</string>
    <string name="identomat_uploading">Uploading...</string>
    <string name="identomat_verifying">Verifying...</string>

    <string name="identomat_upload_instructions_1">Upload a color image of the entire document</string>
    <string name="identomat_upload_instructions_2">JPG or PNG format only</string>
    <string name="identomat_upload_instructions_3">Screenshots are not allowed</string>

    <string name="identomat_document_align">Frame your document</string>
    <string name="identomat_document_blurry">Document is blurry</string>
    <string name="identomat_document_face_blurry">Face on document is blurry</string>
    <string name="identomat_document_face_require_brighter">Low light</string>
    <string name="identomat_document_face_too_bright">Avoid direct light</string>
    <string name="identomat_document_move_away">Please move document away</string>
    <string name="identomat_document_move_closer">Please move document closer</string>
    <string name="identomat_document_move_down">Please move document down</string>
    <string name="identomat_document_move_left">Please move document to the left</string>
    <string name="identomat_document_move_right">Please move document to the right</string>
    <string name="identomat_document_move_up">Please move document up</string>
    <string name="identomat_document_type_unknown">Unknown document type</string>
    <string name="identomat_document_covered">Document is covered</string>
    <string name="identomat_document_grayscale">Document is grayscale</string>
    <string name="identomat_document_format_mismatch">Document format mismatch</string>
    <string name="identomat_document_type_mismatch">Wrong document</string>
    <string name="identomat_document_not_readable">Document not readable</string>
    <string name="identomat_document_face_align">Document face align</string>
    <string name="identomat_document_spoofing_detected2">Document spoofing detected</string>
    <string name="identomat_document_page_mismatch">Wrong page</string>
    <string name="identomat_face_look_straight">Look straight</string>
    <string name="identomat_face_mismatch">Faces do not match</string>


    <string name="identomat_face_align">Frame your face</string>
    <string name="identomat_face_away_from_center">Center your Face</string>
    <string name="identomat_face_blurry">Face is blurry</string>
    <string name="identomat_face_far_away">Move closer</string>
    <string name="identomat_face_require_brighter">Low light</string>
    <string name="identomat_face_too_bright">Avoid direct light</string>
    <string name="identomat_face_too_close">Move away</string>
    <string name="identomat_no_document_in_image">Frame your document</string>
    <string name="identomat_smile_detected">Get neutral face</string>
    <string name="identomat_upload_success">Successfully uploaded!</string>
    <string name="identomat_scan_success">Success!</string>
    <string name="identomat_scan_success_info">Document scanned successfully, Change it to other side.</string>
    <string name="identomat_liveness_success">Liveness completed successfully</string>
    <string name="identomat_camera_permission_title">Can\'t access the camera</string>
    <string name="identomat_camera_permission_text">Permission on camera is restricted, without camera, app can\'t progress forward, please go to settings and check camera permission.</string>
    <string name="identomat_audio_permission_title">Can\'t access the microphone</string>
    <string name="identomat_audio_permission_text">Permission on microphone is restricted, without microphone, call can\'t be made, please go to settings and check audio permission.</string>

    <string name="identomat_no_connection">No internet connection</string>

    <string name="identomat_scan_retry_title">Capture failed</string>
    <string name="identomat_scan_retry_instruction">Please try again in better lighting</string>
    <string name="identomat_scan_retry_again">Try again</string>
    <string name="identomat_scan_retry_cancel">Cancel process</string>

    <string name="identomat_liveness_retry_title">Let\'s try again</string>
    <string name="identomat_liveness_retry_instruction">But first, please take a look at the instructions</string>
    <string name="identomat_liveness_retry_again">Try again</string>
    <string name="identomat_liveness_retry_instruction_1">Place your FACE within OVAL</string>
    <string name="identomat_liveness_retry_instruction_2">Place your FACE within OVAL and follow the on-screen instructions</string>

    <string name="identomat_camera_deny_title">Camera access denied</string>
    <string name="identomat_camera_deny_settings">Allow access</string>
    <string name="identomat_camera_deny_cancel">Cancel process</string>

    <string name="identomat_cascading_instructions">Ready for the task? \n\n When prompted, repeat \n the facial expression shown on the icons: \n\n<b> • Keep neutral face \n • Smile</b> \n\n good luck!</string>
    <string name="identomat_cascading_button">Start Liveness Check</string>
    <string name="identomat_cascading_neutral_face">Keep neutral face</string>
    <string name="identomat_cascading_start">Get ready for liveness check!</string>
    <string name="identomat_cascading_smile">Smile!</string>
    <string name="identomat_cascading_fail">Liveness Failed</string>
    <string name="identomat_cascading_success">That\'s it!</string>





</resources>

```
  



