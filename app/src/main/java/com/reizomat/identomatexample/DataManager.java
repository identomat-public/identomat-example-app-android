package com.reizomat.identomatexample;
import java.util.HashMap;

public class DataManager {

    static HashMap flags = new HashMap() {{
        put("language", "en");
        put("liveness", true);
        put("allow_document_upload", true);
        put("skip_document", false);
        put("skip_face", false);
        put("require_face_document", false);
        put("skip_agreement", false);
        put("server", "computer");

        put("document_type_id", true);
        put("document_type_passport", true);
        put("document_type_driver_license", true);
        put("document_type_residence_license", true);
        put("document_type_non_bio_passport", true);

        put("allow_general_document_capture", true);
        put("allow_general_document_upload", true);

        put("capture_double_page_passport", false);
        put("cascading_identity_verification", false);

    }};


    static HashMap strings = new HashMap(){{
        put("en",  new HashMap<String, String>() {{
            put("identomat_agree", "Yes I agree");
            put("identomat_disagree", "Disagree");
        }});
        put("ru",  new HashMap<String, String>() {{
            put("identomat_agree", "Согласен");
            put("identomat_disagree", "Не согласен");
        }});
        put("es",  new HashMap<String, String>() {{
            put("identomat_agree", "Acepto");
            put("identomat_disagree", "No acepto");
        }});
        put("ka",  new HashMap<String, String>() {{
            put("identomat_agree", "ვეთანხმები");
            put("identomat_disagree", "არ ვეთანხმები");
        }});
        put("uk",  new HashMap<String, String>() {{
            put("identomat_agree", "Согласен");
            put("identomat_disagree", "Не согласен");
        }});
    }};




    static HashMap colors = new HashMap(){{
        put("background_low", "#222222");
        put("background_high", "#2D2D2D");
        put("text_color_header", "#A4A4A4");
        put("text_color", "#676767");
        put("primary_button", "#7848FF");
        put("primary_button_text", "#FFFFFF");
        put("secondary_button", "#222222");
        put("secondary_button_text", "#FFFFFF");
        put("document_outer", "#2D2D2D");
        put("iteration_text", "#FFFFFF");
        put("selector_header", "#A4A4A4");
        put("loading_indicator_background", "#FFFFFF");
    }};


    static HashMap variables = new HashMap<String, Object>(){{
        put("liveness_neutral_icon",  R.drawable.ic_liveness_neutral_icon);
        put("liveness_smile_icon",  R.drawable.ic_liveness_smile_icon);
        put("liveness_neutral_icon_record",  R.drawable.ic_liveness_neutral_icon);
        put("liveness_smile_icon_record",  R.drawable.ic_liveness_smile_icon);

        put("liveness_retry_icon",  null);
        put("scan_retry_icon",  null);
        put("camera_deny_icon",  null);
        put("upload_retry_icon",  null);

        put("liveness_retry_icon_size", 200);
        put("scan_retry_icon_size", 200);
        put("camera_deny_icon_size",  200);
        put("upload_retry_icon_size",  200);

        put("skip_liveness_instructions", false);
        put("liveness_type", 1);
        put("button_corner_radius",  5);
        put("panel_elevation",  1);

        put("title_font", null);
        put("head1_font", null);
        put("head2_font",  null);
        put("body_font",  null);

        put("title_font_size", 20);
        put("head1_font_size", 20);
        put("head2_font_size", 16);
        put("body_font_size",  11);

    }};



}
