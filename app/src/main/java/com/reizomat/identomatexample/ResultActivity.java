package com.reizomat.identomatexample;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.reizomat.identomatexample.databinding.ActivityResultBinding;
import com.reizomat.identomatexample.network.Api;
import org.json.JSONObject;

public class ResultActivity extends AppCompatActivity {

    private ActivityResultBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResultBinding.inflate(getLayoutInflater());
        initResult();
        iniClick();
        setContentView(binding.getRoot());
    }



    private void iniClick(){
        binding.tryAgainButton.setOnClickListener(v -> finish());
    }

    private void initResult(){
        binding.loadingView.setVisibility(View.VISIBLE);
        Api.getResult((state, message) -> {
            binding.loadingView.setVisibility(View.GONE);
            switch(state){
                case SUCCESS:
                    try {
                        String json = new JSONObject(message).toString(2);
                        binding.resultTextView.setText(json);
                    }catch (Exception e){
                        binding.resultTextView.setText(message);
                    }
                    break;
                case NO_CONNECTION:
                    binding.resultTextView.setText("No internet connection");
                    break;
                case FAIL:
                    binding.resultTextView.setText(message);
                    break;
            }
        });
    }
}