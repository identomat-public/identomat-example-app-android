package com.reizomat.identomatexample.network;

public enum State {
    SUCCESS, FAIL, NO_CONNECTION
}
