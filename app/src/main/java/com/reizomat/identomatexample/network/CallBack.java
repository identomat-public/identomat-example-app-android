package com.reizomat.identomatexample.network;

public interface CallBack {
    void response(State state, String message);
}
