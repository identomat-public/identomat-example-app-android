package com.reizomat.identomatexample.network;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.Map;

public class Api {
    private static String TAG = "identomat_app";

    public static String BASE_URL = "https://widget.identomat.com/";
    private static String SESSION_URL = "external-api/begin";
    private static String RESULT_URL = "external-api/result";
    private static String COMPANY_KEY_URL = "company_key=";




    /**
     Do not put company key in the app. This code is only for demonstration.
     **/
    //TODO insert your company key
    private static String COMPANY_KEY = "insert_your_company_key";

    public static String token = "";

    /**
     You have to generate session token on server side and send it to client. Do not put company key in the app. This code is only for demonstration.
    **/
    public static void getToken(Map<String, ?> flags, CallBack callBack){
        String url = BASE_URL + SESSION_URL +"?"+ COMPANY_KEY_URL + COMPANY_KEY;
        url += "&flags=" + new JSONObject(flags).toString();
        Log.i(TAG, "getToken: " + url);
        AndroidNetworking.post(url)
        .setPriority(Priority.HIGH)
        .build()
        .getAsString(new StringRequestListener() {
            @Override
            public void onResponse(String response) {
                try {
                    JsonElement jsonElem  = new JsonParser().parse(response);
                    if (jsonElem.isJsonPrimitive()) {
                        token = jsonElem.getAsString();
                        callBack.response(State.SUCCESS,  "success");
                    } else {
                        callBack.response(State.FAIL,  "");
                    }
                } catch (Exception e) {
                    callBack.response(State.FAIL,  "");
                    Log.i(TAG, "parse error");
                }
            }

            @Override
            public void onError(ANError anError) {
                if (anError != null) {
                    if (anError.getErrorDetail().equals("connectionError")) {
                        callBack.response(State.NO_CONNECTION, "");
                        return;
                    }
                    Log.i(TAG, "onResponse: " + anError.getErrorBody());
                    callBack.response(State.FAIL, anError.toString());
                }
            }
        });
    }

    /**
     You have to get result on server side and send it to client. Do not put company key in the app. This code is only for demonstration.
     **/
    public static void getResult(CallBack callBack) {
        String url = BASE_URL + RESULT_URL + "?" + "session_token=" + token + "&" + COMPANY_KEY_URL + COMPANY_KEY;
        AndroidNetworking.post(url)
        .setPriority(Priority.HIGH)
        .build().getAsString(new StringRequestListener() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);
                callBack.response(State.SUCCESS, response);
            }

            @Override
            public void onError(ANError anError) {
                if (anError != null) {
                    if (anError.getErrorDetail().equals("connectionError")) {
                        callBack.response(State.NO_CONNECTION, "");
                        return;
                    }
                    Log.i(TAG, "onResponse: " + anError.getErrorBody());
                    callBack.response(State.FAIL, anError.getErrorBody().toString());
                }
            }
        });
    }
}