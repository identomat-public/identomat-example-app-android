package com.reizomat.identomatexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import com.reizomat.identomat.IdentomatManager;
import com.reizomat.identomatexample.databinding.ActivityMainBinding;
import com.reizomat.identomatexample.network.Api;
import com.reizomat.identomatexample.network.State;



public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }


    public void start(View v){
        binding.loadingView.setVisibility(View.VISIBLE);
        v.setVisibility(View.GONE);
        Api.getToken(DataManager.flags, (state, message) -> {
            v.setVisibility(View.VISIBLE);
            binding.loadingView.setVisibility(View.GONE);
            if (state == State.SUCCESS) {
                IdentomatManager.INSTANCE.setUp(Api.token);
                setConfig();
                IdentomatManager.INSTANCE.setCallback(() -> {
                    goToResult();
                    return null;
                });
                startActivity(IdentomatManager.INSTANCE.getIdentomatActivity(MainActivity.this));
            } else {
                if(state == State.NO_CONNECTION){
                    Toast.makeText(MainActivity.this, "No Internet connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setConfig(){
        IdentomatManager.INSTANCE.setBaseUrl(Api.BASE_URL + "api/");
        IdentomatManager.INSTANCE.strings(DataManager.strings);
        IdentomatManager.INSTANCE.setColors(DataManager.colors);

        Typeface font = ResourcesCompat.getFont(this, R.font.bpg_glaho);
        DataManager.variables.put("title_font", font);
        DataManager.variables.put("head1_font", font);
        DataManager.variables.put("head2_font", font);
        DataManager.variables.put("body_font",  font);

        IdentomatManager.INSTANCE.setVariables(DataManager.variables);
        IdentomatManager.INSTANCE.setLogo(() -> binding.animationView);
    }

    public void goToResult(){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
    }
}